Part-of-Speech tagger
=====================
To illustrate the use of `PySeqLab package <https://bitbucket.org/A_2/pyseqlab>`__ in training ``part-of-speech tagger`` using `GENIA corpus 3.02 part of speech annotation <http://www.geniaproject.org/genia-corpus/pos-annotation>`__.
Navigate to the ``tutorials`` folder to explore the notebooks for building, training and using ``part-of-speech tagger``. A rendered html version of the tutorial is available on `PySeqLab applications page <http://pyseqlab.readthedocs.io/en/latest/applications.html>`__.