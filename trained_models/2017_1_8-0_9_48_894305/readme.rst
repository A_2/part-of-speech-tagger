Evaluation
===========

To evaluate the performance of the learned model, navigate to ``eval_model`` folder and 
use the ``conlleval.pl`` script on ``dec_testseqs_fold_0_eval.txt`` in a terminal(shell)::

	> ./conlleval.pl -r -d '\t' < dec_testseqs_fold_0_eval.txt
	
Current results (on test set)
------------------------------

::

	processed 47692 tokens with 47692 phrases; found: 47692 phrases; correct: 47038.
	accuracy:  98.63%; precision:  98.63%; recall:  98.63%; FB1:  98.63
	               '': precision:  77.78%; recall:  77.78%; FB1:  77.78  9
	                (: precision: 100.00%; recall: 100.00%; FB1: 100.00  644
	                ): precision:  99.85%; recall:  99.85%; FB1:  99.85  646
	                ,: precision:  99.88%; recall:  99.94%; FB1:  99.91  1687
	                .: precision: 100.00%; recall: 100.00%; FB1: 100.00  1844
	                :: precision:  98.11%; recall: 100.00%; FB1:  99.05  106
	               CC: precision:  99.54%; recall:  99.37%; FB1:  99.46  1742
	               CD: precision:  97.32%; recall:  95.20%; FB1:  96.25  224
	               DT: precision:  99.78%; recall:  99.76%; FB1:  99.77  3694
	               EX: precision:  90.00%; recall: 100.00%; FB1:  94.74  20
	               FW: precision:  98.22%; recall:  95.95%; FB1:  97.08  169
	               IN: precision:  99.76%; recall:  99.86%; FB1:  99.81  6262
	               JJ: precision:  97.12%; recall:  95.51%; FB1:  96.31  4755
	              JJR: precision:  93.24%; recall:  89.61%; FB1:  91.39  74
	              JJS: precision:  96.15%; recall:  92.59%; FB1:  94.34  26
	               LS: precision:  93.33%; recall: 100.00%; FB1:  96.55  15
	               MD: precision: 100.00%; recall: 100.00%; FB1: 100.00  177
	               NN: precision:  98.28%; recall:  99.02%; FB1:  98.65  14235
	              NNP: precision:  97.67%; recall:  95.45%; FB1:  96.55  43
	             NNPS: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
	              NNS: precision:  98.77%; recall:  98.61%; FB1:  98.69  3240
	              PDT: precision:  66.67%; recall:  66.67%; FB1:  66.67  3
	              POS: precision:  94.12%; recall:  88.89%; FB1:  91.43  17
	              PRP: precision:  99.69%; recall: 100.00%; FB1:  99.84  318
	             PRP$: precision: 100.00%; recall: 100.00%; FB1: 100.00  143
	               RB: precision:  98.05%; recall:  97.89%; FB1:  97.97  1231
	              RBR: precision:  92.59%; recall:  83.33%; FB1:  87.72  27
	              RBS: precision:  81.82%; recall:  90.00%; FB1:  85.71  11
	               RP: precision:  66.67%; recall:  66.67%; FB1:  66.67  3
	               TO: precision: 100.00%; recall:  99.87%; FB1:  99.94  790
	               VB: precision:  98.44%; recall:  96.55%; FB1:  97.49  512
	              VBD: precision:  96.36%; recall:  96.26%; FB1:  96.31  988
	              VBG: precision:  95.18%; recall:  97.73%; FB1:  96.44  498
	              VBN: precision:  96.91%; recall:  97.04%; FB1:  96.97  1554
	              VBP: precision:  97.47%; recall:  98.50%; FB1:  97.98  672
	              VBZ: precision:  99.38%; recall:  97.75%; FB1:  98.55  960
	              WDT: precision:  99.65%; recall:  98.60%; FB1:  99.12  282
	               WP: precision: 100.00%; recall:  80.00%; FB1:  88.89  4
	              WP$: precision: 100.00%; recall: 100.00%; FB1: 100.00  7
	              WRB: precision:  96.08%; recall:  96.08%; FB1:  96.08  51
	               ``: precision:  88.89%; recall:  88.89%; FB1:  88.89  9

Note
-----

Originally, the decoded testing and training sequences by the learned model are written to ``dec_testseqs_fold_0.txt``
and ``dec_trainseqs_fold_0.txt`` files respectively. However, the ``conlleval.pl`` script could not be applied directly 
due to space-separator issues in the word observation tokens. To solve this issue and make use of ``conlleval.pl`` script,
we need to restructure the decoded sequences of the model. Hence, in ``train_tagger.py`` module use ``restructure_output``
function. An example of restructuring the model's decoded test sequences output is::

	> in_file = 'dec_testseqs_fold_0.txt'
	> out_file = 'dec_testseqs_fold_0_eval.txt'
	> restructure_output(in_file, out_file, '', "\t")

Same applies for the decoded training sequences.