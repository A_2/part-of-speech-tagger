'''
@author: ahmed allam <ahmed.alam@yale.edu>
'''

import os
from pyseqlab.features_extraction import HOFeatureExtractor
from pyseqlab.ho_crf_ad import HOCRFAD, HOCRFADModelRepresentation
from pyseqlab.workflow import GenericTrainingWorkflow
from pyseqlab.utilities import TemplateGenerator, generate_trained_model, \
                               generate_datetime_str, create_directory
from pos_attr_extractor import POS_AttributeExtractor 
import numpy as np

current_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
dataset_dir = os.path.join(root_dir, 'dataset', 'GENIA_POS')

def load_DataFileParser_options():
    data_parser_options = {}
    data_parser_options['header'] =  ('w', 'pos')
    data_parser_options['y_ref'] = True
    data_parser_options['column_sep'] = "\t"
    data_parser_options['seg_other_symbol'] = None
    return(data_parser_options) 
        

def template_config_2():
    template_generator = TemplateGenerator()
    template_XY = {}
    # generating template for attr_name = w
    template_generator.generate_template_XY('w', ('1-gram:2-gram:3-gram', range(-2,3)), '1-state:2-states', template_XY)
    
    # generating template for the prefix and suffix 
    attr_names = ('prefix', 'suffix')
    for attr_name in attr_names:
        for i in range(2, 6):
            template_generator.generate_template_XY('{}_{}'.format(attr_name, i), ('1-gram', range(0,1)), '1-state', template_XY)
    
    attr_names = ('hasdash', 'hasnum', 'hascap', 'hasallcaps')
    for attr_name in attr_names:
        template_generator.generate_template_XY(attr_name, ('1-gram', range(0,1)), '1-state', template_XY)

    attr_names = ('shape', )
    for attr_name in attr_names:
        template_generator.generate_template_XY(attr_name, ('1-gram:2-gram:3-gram', range(-1,2)), '1-state', template_XY)
    template_Y = template_generator.generate_template_Y('1-state:2-states:3-states')
    return(template_XY, template_Y)

def template_config():
    template_generator = TemplateGenerator()
    template_XY = {}
    # generating template for attr_name = w
    template_generator.generate_template_XY('w', ('1-gram:2-gram:3-gram', range(-2,3)), '1-state:2-states:3-states', template_XY)
     
    # generating template for the prefix and suffix 
    attr_names = ('prefix', 'suffix')
    for attr_name in attr_names:
        for i in range(2, 6):
            template_generator.generate_template_XY('{}_{}'.format(attr_name, i), ('1-gram', range(0,1)), '1-state', template_XY)
     
    attr_names = ('hasdash', 'hasnum', 'hascap', 'hasallcaps')
    for attr_name in attr_names:
        template_generator.generate_template_XY(attr_name, ('1-gram', range(0,1)), '1-state', template_XY)
 
    attr_names = ('shape', )
    for attr_name in attr_names:
        template_generator.generate_template_XY(attr_name, ('1-gram:2-gram:3-gram', range(-1,2)), '1-state', template_XY)
    template_Y = {'Y':()}
    return(template_XY, template_Y)


def revive_learnedmodel(model_dir):
    lmodel = generate_trained_model(os.path.join(model_dir, 'model_parts'), POS_AttributeExtractor)
    return(lmodel)
    
def run_training(optimization_options, template_config, num_seqs=np.inf, profile=False):
    import cProfile
    if(profile):
        local_def = {'optimization_options':optimization_options,
                     'template_config':template_config,
                     'num_seqs':num_seqs
                    }
        global_def = {'train_crfs':train_crfs}
        uid = generate_datetime_str()
        profiling_dir = create_directory('profiling', root_dir)
        cProfile.runctx('train_crfs(optimization_options, template_config, num_seqs=num_seqs)',
                        global_def, local_def, filename = os.path.join(profiling_dir, "timeprofile_{}".format(uid)))
    else:
        return(train_crfs(optimization_options, template_config, num_seqs=num_seqs))

def restructure_output(in_file, out_file, place_holder="PLACE_HOLDER", sep=" "):
    # to use this for restructured decoded sequences files for evaluation using conll eval script
    f_out = open(out_file, 'a')
    with open(in_file, 'r') as f:
        for line in f:
            line = line.rstrip("\n")
            if(line):
                elems = line.rsplit()
                new_line = "".join(elems[:-2]) + sep + place_holder + sep + sep.join(elems[-2:])
            else:
                new_line = ""
            f_out.write(new_line + "\n")
    f_out.close()

def remove_predictions_from_file(in_file, out_file):
    """removes the last column from a decoded file"""
    f_out = open(out_file, 'a')
    with open(in_file, 'r') as f:
        for line in f:
            line = line.rstrip("\n")
            if(line):
                elems = line.rsplit()
                new_line = "\t".join(elems[:2])
            else:
                new_line = ""
            f_out.write(new_line + "\n")
    f_out.close()

def train_crfs(optimization_options, template_config, num_seqs = np.inf):
    crf_model = HOCRFAD
    model_repr = HOCRFADModelRepresentation
    
    # init attribute extractor
    pos_attrextractor = POS_AttributeExtractor()
    # get the attribute description 
    attr_desc = pos_attrextractor.attr_desc
    
    # load templates
    template_XY, template_Y = template_config()
    # init feature extractor
    fextractor = HOFeatureExtractor(template_XY, template_Y, attr_desc)
    # no need for feature filter
    fe_filter = None

    # create a root directory for processing the data
    wd = create_directory('wd', root_dir)
    workflow_trainer = GenericTrainingWorkflow(pos_attrextractor, fextractor, fe_filter, 
                                               model_repr, crf_model,
                                               wd)
    

    # define the data split strategy
    # random split with 90% in training data
    dsplit_options = {'method':'random', 'trainset_size':90, 'num_splits':1}

    # we are going to train using perceptron
    full_parsing = True

    # load data_parser_options
    data_parser_options = load_DataFileParser_options()
    # load data file
    data_file = os.path.join(dataset_dir, "GENIA_dataset_v1.txt")
    data_split = workflow_trainer.seq_parsing_workflow(dsplit_options,
                                                       seq_file=data_file,
                                                       data_parser_options=data_parser_options,
                                                       num_seqs = num_seqs,
                                                       full_parsing = full_parsing)
    trainseqs_id = data_split[0]['train']
    crf_m = workflow_trainer.build_crf_model(trainseqs_id,
                                             "f_0", 
                                             full_parsing=full_parsing)
    model_dir = workflow_trainer.train_model(trainseqs_id, 
                                             crf_m, 
                                             optimization_options)
    trainseqs_info = {seq_id:workflow_trainer.seqs_info[seq_id] for seq_id in trainseqs_id}
    options_train_seqsinfo = {'seqs_info':trainseqs_info,
                              'model_eval':True,
                              'metric':'f1',
                              'file_name':'train_fold_0.txt',
                              'sep':'\t'
                             }
    model_train_perf = workflow_trainer.use_model(model_dir, options_train_seqsinfo) 
    print("F1 score on the training set: ", model_train_perf['f1']) 
        
    # get test set
    testseqs_id = data_split[0]['test']
    testseqs_info = {seq_id:workflow_trainer.seqs_info[seq_id] for seq_id in testseqs_id}
    
    options_test_seqsinfo = {'seqs_info':testseqs_info,
                             'model_eval':True,
                             'metric':'f1',
                             'file_name':'test_fold_0.txt',
                             'sep':'\t'
                            }

    model_test_perf = workflow_trainer.use_model(model_dir, options_test_seqsinfo)
    print("F1 score on the test set: ", model_test_perf['f1']) 
    return((model_dir, model_train_perf, model_test_perf))
                    
if __name__ == "__main__":
    pass
    