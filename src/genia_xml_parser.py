'''
@author: ahmed allam <ahmed.alam@yale.edu>
'''
import os
import re
from lxml import etree

current_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
dataset_dir = os.path.join(root_dir, 'dataset', 'GENIA_POS')


class GENIA_POS_Parser(object):
    """class used to extract the word and part of speech annotation of the sentences in the GENIAcorpus3.02.pos.xml file"""
    
    def __init__(self):
        self.tree = None
        self.lines = "w\tpos \n"
        
    def read_file(self, input_file):
        self.tree = etree.parse(input_file)
    
    def parse_articles(self, out_file):

        root = self.tree.getroot()
        target_elems = ('article', )
        for element in root.iter(*target_elems):
            print("element tag ", element.tag)
            sentences = self.parse_sentences(element)
            print("="*40)
            self.write_to_file(sentences, out_file)
            # clear self.lines
            self.lines = ""
            
    def parse_sentences(self, article_elem):
        lines = self.lines
        for sentence_elem in article_elem.iter('sentence'):
            for w_elem in sentence_elem.iter('w'):
                lines +=  "{}\t{}\n".format(w_elem.text, w_elem.get('c'))
                print('word: {} ; pos: {}'.format(w_elem.text, w_elem.get('c')))
            print("*"*40)
            lines += "\n"
        return(lines)
    
    def write_to_file(self, lines, out_file):
        with open(out_file, 'a') as f:
            f.write(lines)
            
class GENIA_POS_DataGenerator(object):
    def __init__(self):
        self.unwantedmatch = set(["UI/LS", "-/:", "====================", "TI/LS", "AB/LS"])
        
    def parse_file(self, input_file):
        unwantedmatch = self.unwantedmatch
        reg_patt = r"[0-9]+/CD"
        out_file_1 = os.path.join(root_dir, dataset_dir, "GENIA_dataset_v1.txt")
        f_out = open(out_file_1, 'a')
        with open(input_file, 'r') as f:
            for line in f:
                line = line.rstrip("\n")
                if(line in unwantedmatch or re.search(reg_patt, line)):
                    if(line == "===================="):
                        flag = True
                    continue
                else:
                    elems = line.split("/")
                    pos = elems[-1]
                    words = "/".join(elems[:-1])
                    
                    
                    split_pos = pos.split("|")
                    if(len(split_pos)>1):
                        new_line_1 = "{}\t{}\n".format(words,split_pos[0])
                    else:
                        new_line_1 = "{}\t{}\n".format(words,pos)
                        
                    f_out.write(new_line_1)
                    if(line == "./." and flag):
                        f_out.write("\n")
                        flag = False
        f_out.close()

data_file_xml = os.path.join(root_dir, dataset_dir, "GENIAcorpus3.02.pos.xml")
data_file_txt = os.path.join(root_dir, dataset_dir, "GENIAcorpus3.02.pos.txt")
        
def generate_GENIA_dataset_from_xml():
    parser = GENIA_POS_Parser()
    parser.read_file(data_file_xml)
    parser.parse_articles(os.path.join(root_dir, dataset_dir, "GENIA_dataset_parsed_xml"))

def generate_GENIA_dataset_from_txt():
    parser = GENIA_POS_DataGenerator()
    parser.parse_file(data_file_txt)

if __name__ == "__main__":
    #generate_GENIA_dataset()
    generate_GENIA_dataset_from_txt()