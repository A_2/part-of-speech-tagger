'''
@author: ahmed allam <ahmed.alam@yale.edu>
'''
import re
from pyseqlab.attributes_extraction import GenericAttributeExtractor

class POS_AttributeExtractor(GenericAttributeExtractor):
    """class implementing observation functions that generates attributes from tokens/observations"""
    # set of regular expressions used to extract orthographic attributes
    # the features used are the same ones reported in 
    # Tsuruoka et al. paper, 'Developing a Robust Part-Of-Speech tagger for Biomedical Text'

    hasallcaps = r"^[A-Z]+$"
    hasdash = r"-"
    hascap = r"[A-Z]"
    hasnum = r"[0-9]"
    reg_patts = {'hasdash':hasdash,
                 'hasnum': hasnum,
                 'hascap': hascap,
                 'hasallcaps':hasallcaps
                 }

    def __init__(self):
        attr_desc = self.generate_attributes_desc()
        super().__init__(attr_desc)
    
    def generate_attributes_desc(self):
        attr_desc = {}
        attr_desc['w'] = {'description':'the word/token',
                          'encoding':'categorical'
                         }
        attr_desc['shape'] = {'description':'the shape of the word',
                              'encoding':'categorical'
                             }

        for attr_name in self.reg_patts:
            attr_desc[attr_name] = {'description':'Orthographic feature -- {}'.format(attr_name),
                                    'encoding':'categorical'
                                   }
        for i in range(2, 6):
            attr_desc['prefix_{}'.format(i)] = {'description':'prefix of length {} for the word observation'.format(i),
                                                'encoding':'categorical'
                                               }
            attr_desc['suffix_{}'.format(i)] = {'description':'suffix of length {} for the word observation'.format(i),
                                                'encoding':'categorical'
                                               }
        attr_names = ('hasdash', 'hasnum', 'hascap', 'hasallcaps')
        for attr_name in attr_names:
            attr_desc[attr_name] = {'description':'Orthographic feature -- {}'.format(attr_name),
                                    'encoding':'categorical'
                                   }

        return(attr_desc)
    
 
    def generate_attributes(self, seq, boundaries):
        X = seq.X
        observed_attrnames = list(X[1].keys())
        # segment attributes dictionary
        self.seg_attr = {}
        new_boundaries = []
        # create segments from observations using the provided boundaries
        for boundary in boundaries:
            if(boundary not in seq.seg_attr):
                self._create_segment(X, boundary, observed_attrnames)
                new_boundaries.append(boundary)
#         print("seg_attr {}".format(self.seg_attr))
#         print("new_boundaries {}".format(new_boundaries))
        if(self.seg_attr):
            for boundary in new_boundaries:
                self.get_shape(boundary)
                self.get_orthographic_attr(boundary)
                self.get_prefix(boundary)
                self.get_suffix(boundary)

            # save generated attributes in seq
            seq.seg_attr.update(self.seg_attr)
#             print('saved attribute {}'.format(seq.seg_attr))
            # clear the instance variable seg_attr
            self.seg_attr = {}
        return(new_boundaries)
    
    def get_shape(self, boundary):
        """ boundary is tuple (u,v) that marks beginning and end of a segment"""
        segment = self.seg_attr[boundary]['w']
        res = ''
        for char in segment:
            if char.isupper():
                res += 'A'
            elif char.islower():
                res += 'a'
            elif char.isdigit():
                res += 'D'
            else:
                res += '_'

        self.seg_attr[boundary]['shape'] = res
            
    def get_orthographic_attr(self, boundary):
        # orthorgraphic attributes are extracted using the word observation
        segment = self.seg_attr[boundary]['w']
        for attr_name, regexp_patt in self.reg_patts.items():
            r = re.search(regexp_patt, segment)
            if(r):
                self.seg_attr[boundary][attr_name] = '1'
            else:
                self.seg_attr[boundary][attr_name] = '0'
                
    def get_prefix(self, boundary):
        segment = self.seg_attr[boundary]['w']
        attr_name = "prefix"
        len_seg = len(segment)
        # get prefixes of length 1 up to 5
        for i in range(2, 6):
            if(len_seg >= i):
                p = segment[:i]
            else:
                p = ''
            self.seg_attr[boundary][attr_name + "_{}".format(i)] = p
            

    def get_suffix(self, boundary):
        segment = self.seg_attr[boundary]['w']
        len_seg = len(segment)
        attr_name = "suffix"
        # get suffixes of length 1 up to 5
        for i in range(2,6):
            if(len_seg >= i):
                p = segment[len_seg-i:]
            else:
                p = ''
            self.seg_attr[boundary][attr_name + "_{}".format(i)] = p 
      
def example():    
    from pyseqlab.utilities import SequenceStruct
    X = [{'w':'IL-2'}, {'w':'gene'}, {'w':'expression'}, {'w':'and'}, {'w':'NF-kappa'}, {'w':'B'}, {'w':'activation'}, {'w':'through'}, \
         {'w':'CD28'}, {'w':'requires'}, {'w':'reactive'}, {'w':'oxygen'}, {'w':'production'}, {'w':'by'}, {'w':'5-lipoxygenase'}, {'w':'.'}]

    Y = ['NN', 'NN', 'NN', 'CC', 'NN', 'NN', 'NN', 'IN', 'NN', 'VBZ', 'JJ', 'NN', 'NN', 'IN', 'NN']
    seq = SequenceStruct(X, Y)
    attr_extractor = POS_AttributeExtractor()
    print("attr_desc {}".format(attr_extractor.attr_desc))
    print()
    attr_extractor.generate_attributes(seq, seq.get_y_boundaries())
    for boundary, seg_attr in seq.seg_attr.items():
        print("boundary {}".format(boundary))
        print("attributes {}".format(seg_attr))
        print("-"*50)
    print("seg_attr {}".format(seq.seg_attr))
                      
if __name__ == "__main__":
    example()
